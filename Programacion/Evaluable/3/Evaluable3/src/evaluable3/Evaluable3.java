package evaluable3;

import java.util.Scanner;

// Realizado por Alba Marrades Rodríguez.
public class Evaluable3 {

    //HUNDIR LA FLOTA:
    // Menu para elegir la dificultad.
    public static int menu_dificultad() {
        int menu;
        System.out.println("Niveles de dificultad: ");
        System.out.println("1. Fácil.");
        System.out.println("2. Medio.");
        System.out.println("3. Difícil.");
        System.out.println("4. Personalizado.");
        System.out.println("5. Salir.");
        Scanner reader = new Scanner(System.in);
        do {
            menu = reader.nextInt();
            switch (menu) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                default:
                    menu = -1;
                    System.out.println("Opción no válida, vuelva a introducir una opción: ");
                    break;
            }
        } while (menu == -1);
        return menu;
    }

    // Pasa un número a letra. 
    public static char fila(int x) {
        return (char) (x + 65);
    }

    // Pasa una letra a número.
    public static int letra(char letra) {
        return (int) (letra) - 65;
    }

    // Crea el tablero de juego.
    public static char[][] crear_tablero(int alto, int ancho) {
        char tablero[][] = new char[alto][ancho];

        // Introduce el carácter '-' en todas las celdas.
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                tablero[i][j] = '-';
            }
        }
        return tablero;
    }

    // Crea un tablero personalizado.
    public static char[][] crear_tablero_personalizado() {
        // Lee el alto y ancho que se introduzca por teclado.
        Scanner reader = new Scanner(System.in);
        System.out.println("Introduzca el ancho del tablero (Max = 26): ");
        int x = reader.nextInt();
        if (x > 26) {
            do {
                System.out.println("Ancho no válido, introduzca el ancho del tablero (Max = 26): ");
                x = reader.nextInt();
            } while (x > 26);
        }
        System.out.println("Introduzca el alto del tablero (Max = 26): ");
        int y = reader.nextInt();
        if (y > 26) {
            do {
                System.out.println("Alto no válido, introduzca el alto del tablero (Max = 26): ");
                y = reader.nextInt();
            } while (y > 26);
        }

        char[][] tablero = crear_tablero(x, y);
        return tablero;
    }

    // Muestra el tablero de juego.
    public static void mostrar_tablero(char tablero[][]) {
        int alto = tablero.length;
        int ancho = tablero[0].length;
        System.out.print("  ");
        for (int i = 0; i < ancho; i++) {
            if (i > 9) {
                System.out.print(" " + i + " ");
            } else {
                System.out.print(" " + i + "  ");
            }
        }
        System.out.println();
        for (int x = 0; x < alto; x++) {
            System.out.print(fila(x));
            for (int y = 0; y < ancho; y++) {
                System.out.print(" " + " " + tablero[x][y] + " ");
            }
            System.out.print("\n");
        }
    }

    // Devuelve un número aleatorio entre un número que se pase como parámetro y 0. 
    public static int numero_aleatorio(int max) {
        return (int) (Math.random() * (max + 1));
    }

    // Comprueba que la celda está ocupada.
    public static boolean ocupado(char celda) {
        return celda != '-';
    }

    // Dice si todas las celdas que le pases como parámetro están libres. 
    public static boolean libres(char[] celdas) {
        boolean libres = true;
        for (int i = 0; i < celdas.length; i++) {
            libres = libres && !ocupado(celdas[i]);
        }
        return libres;
    }

    // Inserta una lancha en una celda.
    public static char[][] insertar_lancha(char tablero[][], int x, int y) {
        tablero[x][y] = 'L';
        return tablero;
    }

    // Coloca las lanchas en el tablero de forma aleatoria.
    public static char[][] generar_lanchas(char tablero[][], int n) {
        int alto = tablero.length;
        int ancho = tablero[0].length;
        // Cuenta las lanchas que se han metido.
        int contador_lanchas = 0;
        // Se ejecuta mientras el contador_lanchas sea inferior al número de lanchas que queremos meter, asignado en el parámetro n.
        while (contador_lanchas < n) {
            int x = numero_aleatorio(alto - 1);
            int y = numero_aleatorio(ancho - 1);
            // Comprueba que el barco cabe.
            if (y < ancho) {
                char[] celdas = {tablero[x][y]};
                // Comprueba que la celdas estén libres.
                if (libres(celdas)) {

                    // Inserta una lancha en una posición aleatoria.
                    tablero = insertar_lancha(tablero, x, y);
                    // Suma una lancha al contador una vez se ha introducido. 
                    contador_lanchas++;
                }
            }
        }
        return tablero;
    }

    // Inserta un buque en 3 celdas consecutivas.
    public static char[][] insertar_buque(char tablero[][], int x1, int y1, int x2, int y2, int x3, int y3) {
        tablero[x1][y1] = 'B';
        tablero[x2][y2] = 'B';
        tablero[x3][y3] = 'B';
        return tablero;
    }

    // Coloca los buques en el tablero de forma aleatoria en horizontal.
    public static char[][] generar_buques(char tablero[][], int n) {
        int alto = tablero.length;
        int ancho = tablero[0].length;
        int contador_buques = 0;
        // Se ejecuta mientras el contador_buques sea inferior al número de buques que queremos meter, asignado en el parámetro n.
        while (contador_buques < n) {
            int x1 = numero_aleatorio(alto - 1),
                    y1 = numero_aleatorio(ancho - 1),
                    x2 = x1,
                    y2 = y1 + 1,
                    x3 = x1,
                    y3 = y1 + 2;
            // Comprueba que el barco cabe.
            if (y3 < ancho) {
                char[] celdas = {tablero[x1][y1], tablero[x2][y2], tablero[x3][y3]};
                // Comprueba que la celdas estén libres.
                if (libres(celdas)) {

                    // Inserta un buque en una posición aleatoria.
                    tablero = insertar_buque(tablero, x1, y1, x2, y2, x3, y3);
                    // Suma un buque al contador. 
                    contador_buques++;
                }
            }
        }
        return tablero;
    }

    // Inserta un acorazado en 4 celdas consecutivas. 
    public static char[][] insertar_acorazado(char tablero[][], int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {
        tablero[x1][y1] = 'Z';
        tablero[x2][y2] = 'Z';
        tablero[x3][y3] = 'Z';
        tablero[x4][y4] = 'Z';
        return tablero;
    }

    // Coloca los acorazados en el tablero de forma aleatoria en horizontal.
    public static char[][] generar_acorazados(char tablero[][], int n) {
        int alto = tablero.length;
        int ancho = tablero[0].length;
        int contador_acorazados = 0;
        // Se ejecuta mientras el contador_acorazados sea inferior al número de acorazados que queremos meter, asignado en el parámetro n.
        while (contador_acorazados < n) {
            int x1 = numero_aleatorio(alto - 1),
                    y1 = numero_aleatorio(ancho - 1),
                    x2 = x1,
                    y2 = y1 + 1,
                    x3 = x1,
                    y3 = y1 + 2,
                    x4 = x1,
                    y4 = y1 + 3;
            // Comprueba que el barco cabe.
            if (y4 < ancho) {
                char[] celdas = {tablero[x1][y1], tablero[x2][y2], tablero[x3][y3], tablero[x4][y4]};
                // Comprueba que la celdas estén libres.
                if (libres(celdas)) {

                    // Inserta un acorazado en una posición aleatoria.
                    tablero = insertar_acorazado(tablero, x1, y1, x2, y2, x3, y3, x4, y4);
                    // Suma un acorazado al contador. 
                    contador_acorazados++;
                }
            }
        }
        return tablero;
    }

    // Inserta un portaaviones en 5 celdas consecutivas.
    public static char[][] insertar_portaaviones(char tablero[][], int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int x5, int y5) {
        tablero[x1][y1] = 'P';
        tablero[x2][y2] = 'P';
        tablero[x3][y3] = 'P';
        tablero[x4][y4] = 'P';
        tablero[x5][y5] = 'P';
        return tablero;
    }

    // Coloca los portaaviones en el tablero de forma aleatoria en vertical.
    public static char[][] generar_portaaviones(char tablero[][], int n) {
        int alto = tablero.length;
        int ancho = tablero[0].length;
        int contador_portaaviones = 0;
        // Se ejecuta mientras el contador_portaaviones sea inferior al número de portaaviones que queremos meter, asignado en el parámetro n.
        while (contador_portaaviones < n) {
            int x1 = numero_aleatorio(alto - 1),
                    y1 = numero_aleatorio(ancho - 1),
                    x2 = x1 + 1,
                    y2 = y1,
                    x3 = x1 + 2,
                    y3 = y1,
                    x4 = x1 + 3,
                    y4 = y1,
                    x5 = x1 + 4,
                    y5 = y1;
            // Comprueba que el barco cabe.
            if (x5 < alto) {
                char[] celdas = {tablero[x1][y1], tablero[x2][y2], tablero[x3][y3], tablero[x4][y4], tablero[x5][y5]};
                // Comprueba que la celdas estén libres.
                if (libres(celdas)) {

                    // Inserta un portaaviones en una posición aleatoria.
                    tablero = insertar_portaaviones(tablero, x1, y1, x2, y2, x3, y3, x4, y4, x5, y5);
                    // Suma un portaaviones al contador. 
                    contador_portaaviones++;
                }
            }
        }
        return tablero;
    }

    // Introduce todos los barcos en el tablero.
    public static char[][] generar_barcos(char[][] solucion, int l, int b, int a, int p) {
        solucion = generar_portaaviones(solucion, p);
        solucion = generar_acorazados(solucion, a);
        solucion = generar_buques(solucion, b);
        solucion = generar_lanchas(solucion, l);
        return solucion;
    }

    // Calcula las celdas con barcos del tablero.
    public static int celdas_barcos(int l, int b, int a, int p) {
        // l = lanchas, b = buques, a = acorazados, p = portaaviones.
        int celdas_l = l;
        int celdas_b = 3;
        int celdas_a = 4;
        int celdas_p = 5;
        return (l * celdas_l) + (b * celdas_b) + (a * celdas_a) + (p * celdas_p);
    }

    // Inserta un disparo. Introduce 'A' si es agua y 'X' si ha tocado un barco. 
    public static char[][] disparo(char solucion[][], char tablero[][], int x, int y) {

        if (solucion[x][y] == '-') {
            tablero[x][y] = 'A';
            System.out.println("¡Agua!");
        } else {
            tablero[x][y] = 'X';
            System.out.println("¡Tocado!");

        }
        return tablero;
    }

    // Intentos para acertar las posiciones de los barcos. 
    public static char[][] intentos(int n, char tablero[][], char solucion[][], int contador_celdas) {
        String coordenada;
        char letra;
        int y;
        int x;
        int intentos = 0;
        int aciertos = 0;
        // El bucle se ejecuta mientras queden intentos y no se hayan acertado todos los barcos. 
        while (intentos < n && aciertos < contador_celdas) {
            // Se piden por teclado las coordenadas a donde disparar.
            Scanner reader = new Scanner(System.in);
            System.out.println("Introduzca coordenadas (Ejemplo B5): ");
            // Lee la coordenada y la pasa a mayúsculas.
            coordenada = reader.nextLine().toUpperCase();
            // Pasa la parte que tiene el número de la coordenada a entero.
            y = Integer.parseInt(coordenada.substring(1));
            // Pasa la parte que tiene la letra de la coordenada a carácter.
            letra = coordenada.charAt(0);
            // Pasa el carácter a número.
            x = letra(letra);

            // Inserta un disparo.
            tablero = disparo(solucion, tablero, x, y);
            // Suma un intento al contador.
            intentos++;
            // Comprueba que se haya disparado a un barco.
            if (tablero[x][y] == 'X') {
                // Suma un acierto al contador.
                aciertos++;
            }

            mostrar_tablero(tablero);

        }
        // Comprueba que se hayan acertado todas las celdas donde hay barcos y te dice si has ganado o no.
        if (aciertos == contador_celdas) {
            System.out.println("¡Ha ganado!");
        } else {
            System.out.println("¡Lo siento!, ha perdido, vuelva a intentarlo.");
        }
        return tablero;
    }

    // Copia los disparos del tablero al tablero de la solución.
    public static char[][] calcular_solucion(char[][] tablero, char[][] solucion) {
        int alto = tablero.length;
        int ancho = tablero[0].length;
        for (int x = 0; x < alto; x++) {
            for (int y = 0; y < ancho; y++) {
                if (tablero[x][y] == 'X' || tablero[x][y] == 'A') {
                    solucion[x][y] = tablero[x][y];
                }
            }
        }
        return solucion;
    }

    public static void main(String[] args) {
        char tablero[][];
        char solucion[][];
        int menu;
        int contador_celdas;
        do {
            menu = menu_dificultad();
            switch (menu) {

                // Dificultad: Fácil.
                case 1:
                    // Crea el tablero con las medidas asignadas.
                    tablero = crear_tablero(10, 10);
                    // Crea el tablero donde introduciremos los barcos.
                    solucion = crear_tablero(10, 10);

                    // Introduce los barcos. 
                    solucion = generar_barcos(solucion, 5, 3, 1, 1);

                    // Cuenta las celdas ocupadas con barcos.
                    contador_celdas = celdas_barcos(5, 3, 1, 1);
                    // Muestra el tablero.
                    mostrar_tablero(tablero);

                    // Intentos para acertas las posiciones de los barcos.
                    tablero = intentos(50, tablero, solucion, contador_celdas);

                    // Introduce los disparos en el tablero de solución.
                    solucion = calcular_solucion(tablero, solucion);
                    // Muestra la solución.
                    mostrar_tablero(solucion);
                    break;

                // Dificultad : Media.
                case 2:
                    tablero = crear_tablero(10, 10);
                    solucion = crear_tablero(10, 10);

                    solucion = generar_barcos(solucion, 2, 1, 1, 1);

                    contador_celdas = celdas_barcos(2, 1, 1, 1);
                    mostrar_tablero(tablero);

                    tablero = intentos(30, tablero, solucion, contador_celdas);

                    solucion = calcular_solucion(tablero, solucion);
                    mostrar_tablero(solucion);
                    break;

                // Dificultad : Difícil.
                case 3:
                    tablero = crear_tablero(10, 10);
                    solucion = crear_tablero(10, 10);

                    solucion = generar_barcos(solucion, 1, 1, 0, 0);

                    contador_celdas = celdas_barcos(1, 1, 0, 0);
                    mostrar_tablero(tablero);

                    tablero = intentos(10, tablero, solucion, contador_celdas);

                    solucion = calcular_solucion(tablero, solucion);
                    mostrar_tablero(solucion);
                    break;

                // Dificultas : Personalizado.
                case 4:
                    Scanner reader = new Scanner(System.in);

                    // Crea un tablero con las medidas introducias.
                    tablero = crear_tablero_personalizado();
                    int alto = tablero.length;
                    int ancho = tablero[0].length;
                    solucion = crear_tablero(alto, ancho);

                    // Pide los barcos a introducir.
                    System.out.println("¿Cuántos portaaviones quiere introducir?: ");
                    int p = reader.nextInt();
                    System.out.println("¿Cuántos acorazados quiere introducir?: ");
                    int a = reader.nextInt();
                    System.out.println("¿Cuántos buques quiere intoducir?: ");
                    int b = reader.nextInt();
                    System.out.println("¿Cuántas lanchas quiere intoducir?: ");
                    int l = reader.nextInt();
                    // Introduce en el tablero los barcos que se han metido por teclado.
                    solucion = generar_barcos(solucion, l, b, a, p);
                    // Cuenta las celdas ocupadas con barcos.

                    contador_celdas = celdas_barcos(p, a, b, l);
                    // Muestra el tablero de juego.
                    mostrar_tablero(tablero);

                    // Pide los intentos.
                    System.out.println("¿Cuántos intentos quiere tener?: ");
                    int intentos = reader.nextInt();
                    // Ejecuta los intentos que se han introducido.
                    tablero = intentos(intentos, tablero, solucion, contador_celdas);

                    // Calcula y muestra la solución.
                    solucion = calcular_solucion(tablero, solucion);
                    mostrar_tablero(solucion);
            }
        } while (menu != 5);
    }
}
