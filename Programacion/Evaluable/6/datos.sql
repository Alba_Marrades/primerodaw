-- Crear la BD 

CREATE DATABASE bd CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE bd;

-- Crear usuario alumno

CREATE USER 'alumno'@'localhost'
IDENTIFIED BY 'alumno';

-- Darle privilegios

GRANT ALL PRIVILEGES ON bd . * TO 'alumno'@'localhost';
FLUSH PRIVILEGES;

-- Crear tabla alumnos

CREATE TABLE alumnos(
	idalumno INT AUTO_INCREMENT,
	nombre VARCHAR(60),
	email VARCHAR(120),
	telefono VARCHAR (12),
	PRIMARY KEY (idalumno )
);

-- Rellenar resgistros

INSERT INTO alumnos (nombre, email, telefono)
VALUES ('Fernando Castillo Perez', 'castillo.fernando@gmail.com', '684527415'),
	('Maria Gimenez Garnelo', 'gimenez.maria@gmail.com', '678251478');


-- Crear tabla citas(

CREATE TABLE citas(
	idcita INT AUTO_INCREMENT,
	fecha DATE,
	hora TIME,
	idalumno INT, 
	PRIMARY KEY (idcita),
	FOREIGN KEY (idalumno) REFERENCES alumnos(idalumno )
	ON DELETE CASCADE
);

-- Rellenar registros

INSERT INTO citas (fecha, hora, idalumno)
VALUES ('2021-04-06', '20:09', 1),
	('2021-05-20', '16:20', 2);