package evaluable2;

import java.util.Scanner;

// Realizado por Alba Marrades Rodríguez. 
public class Evaluable2 {

    /*SOPA DE LETRAS DE FRUTAS: 
    Tabla de 15x15 de letras mayúsculas. 
    15 palabras almacenadas, cuya longitud máxima no supere el tamaño de 15 carácteres, puestas en la tabla. 
    las palabras se ponen de forma vertical u horizontal aleatoriamente. Una vez puestas las palabras se acaba de rellenar con letras aleatorias.*/
    public static void main(String[] args) {
        //Palabras de la sopa. 
        String frutas[] = {"MANZANA", "NARANJA", "CIRUELA", "KIWI", "POMELO", "PERA", "MELON", "MORA", "LIMON", "FRESA", "COCO", "HIGO",
            "MANGO", "PIÑA", "UVA"};
        // letras para rellenar los huecos sobrantes.
        char letras[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        //tabla de la sopa. 
        char[][] tabla = new char[15][15];
        //Se usa para numerar la lista de frutas y lleva la cuenta de las frutas que se han metido. 
        int contador = 1;
        Scanner reader = new Scanner(System.in);
        //Pone todos los valores del array con un punto.
        for (int x = 0; x < 15; x++) {
            for (int y = 0; y < 15; y++) {
                tabla[x][y] = '.';
            }
        }
        do {
            // variable para intercambiar entre posición vertical y horizontal. 
            boolean girar = false;
            for (int fila = 0; fila < 15; fila++) {
                // Si frutas[fila] es null, la palabra ya se ha introducido.
                if (frutas[fila] == null) {
                    continue;
                }
                // Longitud de la palabra. 
                int long_palabra = frutas[fila].length();
                // Selecciona una columna aleatoria de la tabla. 
                int columna_aleatoria = (int) (Math.random() * 15);
                // string de la palabra seleccionada del array. 
                String palabra = frutas[fila];
                // variable para que el programa no meta una palabra en una celda ya ocupada.
                boolean ocupado = false;

                /* Comprueba si la palabra cabe en la horizontal antes de meterla. Para que se vaya intercambiando entre la vertical
                y la horizontal una vez girar será false y la siguiente true.*/
                if (long_palabra + columna_aleatoria < 15 && girar == false) {
                    for (int i = 0; i < palabra.length(); i++) {
                        //comprueba que la celda no está ocupada. 
                        if (tabla[fila][columna_aleatoria + i] != '.') {
                            ocupado = true;
                        }
                    }
                    if (!ocupado) {
                        // la i recorre las columnas de una fila para meter los carácteres de la palabra en cada celda. 
                        for (int i = 0; i < palabra.length(); i++) { 
                            // Introduce la palabra en la horizontal. 
                            tabla[fila][columna_aleatoria + i] = palabra.charAt(i); 
                        }
                        // Imprime la palabra introducida. 
                        System.out.println(contador + ". " + frutas[fila]); 
                        contador++;
                        // Cambia el valor de la palabra introducida a null. 
                        frutas[fila] = null; 
                        // cambia a true la variable girar para que la siguiente palabra se coloque en vertical. 
                        girar = true; 
                    }

                    //comprueba que cabe en la vertical.
                } else if (long_palabra + fila < 15) { 
                    for (int i = 0; i < palabra.length(); i++) {
                        //comprueba que la celda no está ocupada.
                        if (tabla[fila + i][columna_aleatoria] != '.') { 
                            ocupado = true;
                        }
                    }
                    if (!ocupado) {
                        // La i recorre las filas de una columna para meter los carácteres de la palabra en cada celda. 
                        for (int i = 0; i < palabra.length(); i++) { 
                            //Introduce la palabra en la vertical. 
                            tabla[fila + i][columna_aleatoria] = palabra.charAt(i); 
                        }
                        // Imprime la palabra introducida. 
                        System.out.println(contador + ". " + frutas[fila]); 
                        contador++;
                        // Cambia el valor de la palabra introducida a null.
                        frutas[fila] = null; 
                        // cambia a false la variable para que la siguiente palabra se coloque en horizontal. 
                        girar = false; 
                    }
                }

            }

            /* El bucle termina cuando el contador es mayor que 15. La variable contador va contando las palabras introducidas,
            que van tomando el valor null cuando el programa las coloca.*/
        } while (contador <= 15); 

        // Tabla que muestra la solución. 
        char tabla_solucion[][] = new char[15][15]; 

        //Bucle para introducir letras aleatorias en los huecos sin palabras. 
        for (int x = 0; x < 15; x++) { 
            for (int y = 0; y < 15; y++) {
                // Copia la tabla en la tabla_solucion antes de introducir las letras. 
                tabla_solucion[x][y] = tabla[x][y]; 
                if (tabla[x][y] == '.') {
                    tabla[x][y] = letras[(int) (Math.random() * 26)];
                }

            }
        }

        //Bucle para imprimir la sopa de letras. 
        for (int x = 0; x < 15; x++) { 
            for (int y = 0; y < 15; y++) {
                System.out.print(tabla[x][y] + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        System.out.print("\n");
        System.out.println("Pulse cualquier tecla para mostrar la solución.");
        //Lee la tecla. 
        reader.nextLine(); 
        // Muestra la tabla con la solución. 
        for (int x = 0; x < 15; x++) { 
            for (int y = 0; y < 15; y++) {
                System.out.print(tabla_solucion[x][y] + " ");
            }
            System.out.print("\n");

        }
    }
}
