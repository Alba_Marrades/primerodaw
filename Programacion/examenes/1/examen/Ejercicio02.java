/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen;

import java.util.Scanner;

public class Ejercicio02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n, suma = 0;
        Scanner reader = new Scanner(System.in);
        System.out.println("Introduzca un número entero: ");
        n = reader.nextInt();
        while (n != 0) {
            suma += n;
            System.out.println("Introduzca un número entero: ");
            n = reader.nextInt();
        }
        System.out.println(suma);
    }
}
