﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template match="/">
    <html>
      <head>
        <link rel="stylesheet" href="css/style.css"/> 
      </head>  
      <body>
        <header>
          <h1>LISTA DE JUGADORES</h1>        
        </header>      
        <main>
          <table>
            <tr>
              <th>Nombre</th>
              <th>Altura</th>
              <th>Posición</th>            
            </tr>     
            <xsl:for-each select="catalog/jugador">
              <xsl:choose>
                <xsl:when test="altura &gt; 1.90">
                  <tr>
                    <td><xsl:value-of select="nombre"/></td>  
                    <td bgcolor="#ff00ff"><xsl:value-of select="altura"/></td>         
                    <td><xsl:value-of select="posicion"/></td>         
                  </tr>                
                </xsl:when>    
                <xsl:otherwise>
                  <td><xsl:value-of select="nombre"/></td>  
                  <td bgcolor="#e5e749"><xsl:value-of select="altura"/></td>         
                  <td><xsl:value-of select="posicion"/></td> 
                </xsl:otherwise>          
              </xsl:choose>            
            </xsl:for-each>     
          </table>        
        </main>
      </body>  
    </html>
  </xsl:template>
  
</xsl:stylesheet>
