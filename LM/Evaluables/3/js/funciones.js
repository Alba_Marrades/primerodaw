/**
 * Punto de inicio de la aplicación
 */
function procesar() {
  // Parámetros de la URL
  const queryString = window.location.search;
  // Convertir parámetros en objeto de tipo URLParams
  const urlParams = new URLSearchParams(queryString);
  // Por defecto 12 pokemons
  var nPokemons = 12;
  // Si hay parámetro nPokemons = parámetro
  if (urlParams.has("nPokemons")) {
    nPokemons = urlParams.get("nPokemons");
  }
  // Llamar a la función crear galería
  crearGaleria(nPokemons);
}

function urlImagen(numeroImagen) {
  return "https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + numeroImagen + ".png";
}

/**
 * Función que crea los items de la galería de pokemons
 * @param {Número de pokemons} numero
 */
function crearGaleria(numero) {
  // Objeto para hacer llamadas Http
  var xmlhttp = new XMLHttpRequest();
  // Cuando cambia el estado del objeto
  xmlhttp.onreadystatechange = function () {
    // Cuando el estado sea 200 (OK)
    if (this.readyState == 4 && this.status == 200) {
      // Convertir la respuesta de la llamada en JSON
      var data = JSON.parse(this.responseText);
      // Lista de resultados de la respuesta
      var pokemons = data.results;
      console.log(pokemons);
      // String html
      var htmlPokemon = "";
      var urlPokemon = "'https://pokeapi.co/api/v2/pokemon/'";
      // Recorrer pokemons
      for (var x in pokemons) {
        var nombreImagen = parseInt(x) + 1;
        if (nombreImagen <= 9) {
          nombreImagen = "00" + nombreImagen;
        } else if (nombreImagen >= 10 && nombreImagen <= 99) {
          nombreImagen = "0" + nombreImagen;
        }
        htmlPokemon +=
          '<div class="contenedor-columna"><div class="center pokemon">';
        htmlPokemon +=
          '<a href="#miModal" onclick="obtenerPokemon(' +
          urlPokemon +
          "," +
          (parseInt(x) + 1) +
          ",'" +
          nombreImagen +
          '\')"><img src="' + urlImagen(nombreImagen) +
          '" alt="Imágen de el pokemon ' +
          pokemons[x].name +
          '">';
        htmlPokemon += "<p>" + pokemons[x].name + "</p></a></div></div>";
        console.log(nombreImagen);  
      }
      // Añade el string html al elemento galería
      document.getElementById("galeria").innerHTML = htmlPokemon;
    }
  };
  // Configura URL y método
  xmlhttp.open(
    "GET",
    "https://pokeapi.co/api/v2/pokemon/?limit=" + numero,
    true
  );
  // Hace la llamada
  xmlhttp.send();
}

/**
 *
 * @param {url de la API} url
 * @param {Numero del pokemon actual} numeroPokemon
 * @param {Número de la imagen} numeroImagen
 */
function obtenerPokemon(url, numeroPokemon, numeroImagen) {
  // Objeto para hacer llamadas Http
  var xmlhttp = new XMLHttpRequest();
  // Cuando cambia el estado del objeto
  xmlhttp.onreadystatechange = function () {
    // Cuando el estado sea 200 (OK)
    if (this.readyState == 4 && this.status == 200) {
      // Convertir la respuesta de la llamada en JSON
      var data = JSON.parse(this.responseText);
      console.log(data);
      document.getElementById("nombre").innerHTML = " " + data.name;
      document.getElementById("peso").innerHTML = " " + data.weight;
      document.getElementById("altura").innerHTML = " " + data.height;
      
      var habilidades = "";
      // Bucle para recorrer las habilidades.
      for (i in data.abilities) {
        habilidades += data.abilities[i].ability.name;
        if (i < data.abilities.length - 1) {
          habilidades += " ";
        }
      }

      document.getElementById("habilidades").innerHTML = habilidades;
      var imagenPokemon = urlImagen(numeroImagen);
      document.getElementById("imagenPokemon").style.backgroundImage = "url(" + imagenPokemon + ")";
    }
  };
  // Configura URL y método
  xmlhttp.open("GET", url + numeroPokemon, true);
  // Hace la llamada
  xmlhttp.send();
}
