﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template match="/">
    <html>
      <body>
        <h1>My Plants</h1>
          <table border="1">
            <tr bgcolor="#4297E8">
              <th style="text-align:left">Common</th>
              <th style="text-align:left">Botanical</th>            
            </tr>   
            <xsl:for-each select="catalog/plant">
              <xsl:if test="light = 'Sunny'">
                <tr>
                  <td><xsl:value-of select="common"/></td>
                  <td><xsl:value-of select="botanical"/></td> 
                </tr>
              </xsl:if>            
            </xsl:for-each>       
          </table>      
      </body>    
    </html>
  </xsl:template>
  
</xsl:stylesheet>
