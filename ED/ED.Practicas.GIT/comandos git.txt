git init (crear un repositorio)
git config --global user.name o user.email (identificarse)
git remote add origin URL (Indicar la dirección de origen)
git pull (Bajar de la nube a local)
git push (Subir de local a la nube) git push -u origin master
git add . (añadir contenido a el área de preparación/staging area)
git commit -m "" (incorporar los cambios al repositorio)
git log --oneline (Listar los cambios(Commits) que se han producido en el repositorio)
