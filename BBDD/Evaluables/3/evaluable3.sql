CREATE TABLESPACE evaluable3
DATAFILE 'C:\oraclexe\app\oracle\oradata\XE\evaluable3.dbf'
SIZE 50M AUTOEXTEND ON NEXT 50M MAXSIZE 300M
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 10M
SEGMENT SPACE MANAGEMENT AUTO;


CREATE USER evaluable3 IDENTIFIED BY evaluable3
DEFAULT TABLESPACE evaluable3;

GRANT CONNECT, RESOURCE, EXP_FULL_DATABASE, IMP_FULL_DATABASE TO evaluable3;
CONNECT evaluable3/evaluable3;

-- Crear Tablas

CREATE TABLE usuarios (
	dni VARCHAR(9) PRIMARY KEY,
	nombre VARCHAR(25),
	apellido1 VARCHAR(20),
	apellido2 VARCHAR(20) NULL,
	password VARCHAR(8)	
);
	
CREATE TABLE alumnos (
	dni VARCHAR(9) PRIMARY KEY,	
	fecha_nacimiento DATE,
	FOREIGN KEY(dni) REFERENCES usuarios(dni)
);

CREATE TABLE departamentos (
	departamento VARCHAR(20) PRIMARY KEY,	
	jefe_dep VARCHAR(9)
	-- No se puede declarar la FK puesto que aún no existe la tabla profesores
	-- FOREIGN KEY(jefe_dep) REFERENCES profesores(dni)
);

CREATE TABLE profesores (
	dni VARCHAR(9) PRIMARY KEY,	
	departamento VARCHAR(20),
	FOREIGN KEY(dni) REFERENCES usuarios(dni),
	FOREIGN KEY(departamento) REFERENCES departamentos(departamento)
);

-- Ahora ya podríamos activar la Caj en departamentos, pero con los profesores y
-- departamentos vacíos y ambas relacionadas entre ellas con Caj nos obliga a declarar
-- las Caj como null al insertar profesores y modificar las tuplas cuando estén los
-- departamentos o viceversa, así que ya la activaremos cuando estén insertados los
-- datos con la siguiente orden:
-- ALTER TABLE departamentos ADD(FOREIGN KEY(jefe_dep) REFERENCES profesores(dni));

CREATE TABLE modulos (
	cod_mod VARCHAR(4) PRIMARY KEY,	
	modulo VARCHAR(35)
);

CREATE TABLE imparten (
	dni_profesor VARCHAR(9),	
	cod_mod VARCHAR(4),
	PRIMARY KEY(dni_profesor,cod_mod),
	FOREIGN KEY(dni_profesor) REFERENCES profesores(dni),
	FOREIGN KEY(cod_mod) REFERENCES modulos(cod_mod)
);

CREATE TABLE matriculados (
	dni_alumno VARCHAR(9),	
	cod_mod VARCHAR(4),
	PRIMARY KEY(dni_alumno,cod_mod),
	FOREIGN KEY(dni_alumno) REFERENCES alumnos(dni),
	FOREIGN KEY(cod_mod) REFERENCES modulos(cod_mod)
);
	
CREATE TABLE ti (
	id_ti VARCHAR(4),
	dia INTEGER CHECK(dia>1 AND dia<7),
	hora DATE,
	dni_profesor VARCHAR(9),
	PRIMARY KEY(id_ti),
	unique (dia,hora,dni_profesor),
	FOREIGN KEY(dni_profesor) REFERENCES profesores(dni)
);

CREATE TABLE reserva_ti(
	fecha_ti DATE,
	id_ti VARCHAR(4),
	dni_alumno VARCHAR(9),
	PRIMARY KEY(fecha_ti,id_ti),
	FOREIGN KEY(dni_alumno) REFERENCES alumnos(dni),
	FOREIGN KEY(id_ti) REFERENCES ti(id_ti)
);

CREATE TABLE aulas (
	cod_aula VARCHAR(3) PRIMARY KEY,
	tipo VARCHAR(11) CHECK(tipo IN ('informatica','estudio','taller','laboratorio')),
	capacidad_max INTEGER
);

CREATE TABLE reserva_aula (
	cod_aula VARCHAR(3),
	dni_profesor VARCHAR(9),
	fecha DATE,
	PRIMARY KEY (cod_aula,fecha),
	FOREIGN KEY (dni_profesor) REFERENCES profesores(dni)
);