CREATE USER exam2ev IDENTIFIED BY exam2ev;
GRANT CONNECT, RESOURCE TO exam2ev;
CONNECT  exam2ev/exam2ev;

-- Crear Tablas

CREATE TABLE usuarios (
	usuario VARCHAR(6) PRIMARY KEY,
	nif VARCHAR(9) UNIQUE,
	nombre VARCHAR(25),
	apellido1 VARCHAR(20),
	apellido2 VARCHAR(20) NULL,
	ciudad varchar(30),
	balance INT,
	password VARCHAR(8)
);

CREATE TABLE libros (     
	id_libro VARCHAR(7) PRIMARY KEY,
	nombre VARCHAR(75) UNIQUE,
	autor VARCHAR (40),
	cantidad INT,
	calificacion FLOAT
);

CREATE TABLE cede (         
	id_libro VARCHAR(10),
	usuario VARCHAR(6),
	cantidad INT,
	calificacion FLOAT,
	PRIMARY KEY (id_libro,usuario),
	FOREIGN KEY(id_libro) REFERENCES libros(id_libro),
	FOREIGN KEY(usuario) REFERENCES usuarios(usuario)
);

CREATE TABLE pide (         
	id_libro VARCHAR(10),
	usuario VARCHAR(6),
	fecha DATE,
	calificacion FLOAT,
	PRIMARY KEY (id_libro,usuario),
	FOREIGN KEY(id_libro) REFERENCES libros(id_libro),
	FOREIGN KEY(usuario) REFERENCES usuarios(usuario)
);

INSERT INTO usuarios VALUES ('JUAPAS','31647113X','JUAN JOSE','PASCUAL','VILLAR','Valencia',5,'jK1kL3fI');
INSERT INTO usuarios VALUES ('FRASUA','81666058G','FRANCISCO','SUAREZ','MOLINA','Valencia',5,'gQ2lK1mB');
INSERT INTO usuarios VALUES ('ANANAV','89939657C','ANA','NAVARRO','DIAZ','Valencia',5,'gQmGgDlL');
INSERT INTO usuarios VALUES ('JUALOR','13410741Q','JUAN ANTONIO','LORENZO','MATEO','Elche',5,'bFlCcCqO');
INSERT INTO usuarios VALUES ('CARGAL','45393627E','CARMEN','GALLEGO','CARRILLO','Alicante',5,'fqifbdof');
INSERT INTO usuarios VALUES ('JOSSAN','81255212F','JOSE','SANTANA','DELGADO','Alicante',5,'vF3lK5bE');
INSERT INTO usuarios VALUES ('JESMEN','90227369W','JESUS','MENDEZ','BENITO','Valencia',5,'oD1nD1cH');
INSERT INTO usuarios VALUES ('ALBFRA','26330456X','ALBERTO','FRANCO','CAMACHO','Valencia',5,'gbhedbdk');
INSERT INTO usuarios VALUES ('IREBLA','01744939K','IRENE','BLANCO','MARTIN','Castellon',5,'khldcmeg');
INSERT INTO usuarios VALUES ('LUIPAR','00097347B','LUIS','PARRA','SANZ','Valencia',5,'fG2hB7bC');
INSERT INTO usuarios VALUES ('DOLGAR','05486504S','DOLORES','GARCIA','MONTES','Castellon',5,'33321332');
INSERT INTO usuarios VALUES ('MANGUE','76847778V','MANUEL','GUERRERO','MARIN','Valencia',5,'abcdefgh');
INSERT INTO usuarios VALUES ('LUCHER','90011195M','LUCIA','HERRERA','TORRES','Elche',5,'abcdefgh');
INSERT INTO usuarios VALUES ('ADRRED','52704314K','ADRIAN','REDONDO','RODRIGUEZ','Valencia',5,'dR6dC1mC');
INSERT INTO usuarios VALUES ('ROSIBA','29318262R','ROSARIO','IBA�EZ','MENDEZ','Alicante',5,'26330456');

INSERT INTO libros VALUES ('BFD8992','Las mil y una noches',NULL,5,NULL);
INSERT INTO libros VALUES ('DLT9758','Moby Dick','Herman Melville',5,NULL);
INSERT INTO libros VALUES ('FJM4476','Hojas de hierba','Walt Whitman',5,NULL);
INSERT INTO libros VALUES ('FNV4363','Crimen y castigo','Fiodor Dostoievski',5,NULL);
INSERT INTO libros VALUES ('FWW4785','Los hermanos Karamazov','Fiodor Dostoievski',5,NULL);
INSERT INTO libros VALUES ('JTS5457','El ruido y la furia','William Faulkner',5,NULL);
INSERT INTO libros VALUES ('KBA3785','Madame Bovary','Gustave Flaubert',5,NULL);
INSERT INTO libros VALUES ('LVN2376','El coronel no tiene quien le escriba','Gabriel Garcia Marquez',5,NULL);
INSERT INTO libros VALUES ('PNZ3225','Fausto','Goethe',5,NULL);
INSERT INTO libros VALUES ('PQS2578','Grandes Esperanzas','Charles Dickens',5,NULL);
INSERT INTO libros VALUES ('PVB6564','El viejo y el mar','Ernest Hemingway',5,NULL);
INSERT INTO libros VALUES ('RVG4939','Iliada ','Homero',5,NULL);
INSERT INTO libros VALUES ('RVJ7973','Poema de Gilgamesh',NULL,5,NULL);
INSERT INTO libros VALUES ('SFN6569','Odisea ','Homero',5,NULL);
INSERT INTO libros VALUES ('SXW8859','El Hobbit','J. R. R. Tolkien',5,NULL);
INSERT INTO libros VALUES ('WRG9255','Ulises','James Joyce',5,NULL);
INSERT INTO libros VALUES ('XEM5729','Hamlet','William Shakespeare',5,NULL);
INSERT INTO libros VALUES ('YWQ3299','El rey Lear','William Shakespeare',5,NULL);
INSERT INTO libros VALUES ('ZNL9675','Otelo','William Shakespeare',5,NULL);
INSERT INTO libros VALUES ('ZVJ5656','1984','George Orwell',5,NULL);

INSERT INTO cede VALUES ('XEM5729','JESMEN',1,1);
INSERT INTO cede VALUES ('PQS2578','CARGAL',1,4);
INSERT INTO cede VALUES ('RVG4939','LUCHER',1,3);
INSERT INTO cede VALUES ('PVB6564','LUCHER',2,0);
INSERT INTO cede VALUES ('FNV4363','CARGAL',1,4);
INSERT INTO cede VALUES ('SXW8859','JUAPAS',4,2);
INSERT INTO cede VALUES ('SFN6569','CARGAL',1,0);
INSERT INTO cede VALUES ('KBA3785','ROSIBA',1,5);
INSERT INTO cede VALUES ('BFD8992','JUAPAS',1,5);
INSERT INTO cede VALUES ('XEM5729','JUALOR',2,5);
INSERT INTO cede VALUES ('RVJ7973','ALBFRA',1,1);
INSERT INTO cede VALUES ('ZNL9675','JUALOR',1,5);
INSERT INTO cede VALUES ('SXW8859','FRASUA',1,5);
INSERT INTO cede VALUES ('FNV4363','JUALOR',1,3);
INSERT INTO cede VALUES ('WRG9255','JUALOR',1,1);
INSERT INTO cede VALUES ('KBA3785','DOLGAR',1,0);
INSERT INTO cede VALUES ('RVG4939','DOLGAR',1,5);
INSERT INTO cede VALUES ('PNZ3225','MANGUE',1,0);
INSERT INTO cede VALUES ('BFD8992','IREBLA',2,4);
INSERT INTO cede VALUES ('FJM4476','LUIPAR',1,3);
INSERT INTO cede VALUES ('FNV4363','ANANAV',1,3);
INSERT INTO cede VALUES ('WRG9255','JESMEN',1,3);
INSERT INTO cede VALUES ('PVB6564','JESMEN',1,2);
INSERT INTO cede VALUES ('LVN2376','ANANAV',1,5);
INSERT INTO cede VALUES ('WRG9255','MANGUE',1,1);
INSERT INTO cede VALUES ('DLT9758','LUIPAR',1,5);
INSERT INTO cede VALUES ('LVN2376','JUAPAS',1,1);
INSERT INTO cede VALUES ('ZNL9675','FRASUA',1,5);
INSERT INTO cede VALUES ('LVN2376','ALBFRA',1,4);
INSERT INTO cede VALUES ('BFD8992','JUALOR',1,4);
INSERT INTO cede VALUES ('PNZ3225','FRASUA',2,2);
INSERT INTO cede VALUES ('SFN6569','JUAPAS',1,5);
INSERT INTO cede VALUES ('FWW4785','LUIPAR',1,1);
INSERT INTO cede VALUES ('FWW4785','ANANAV',1,3);
INSERT INTO cede VALUES ('YWQ3299','IREBLA',1,2);
INSERT INTO cede VALUES ('KBA3785','JUAPAS',1,1);
INSERT INTO cede VALUES ('WRG9255','ANANAV',3,2);
INSERT INTO cede VALUES ('YWQ3299','JOSSAN',1,3);
INSERT INTO cede VALUES ('SFN6569','JUALOR',1,5);
INSERT INTO cede VALUES ('PVB6564','JUAPAS',1,4);
INSERT INTO cede VALUES ('RVG4939','JUAPAS',2,3);
INSERT INTO cede VALUES ('SXW8859','JESMEN',1,4);

INSERT INTO pide VALUES ('KBA3785','IREBLA',TO_DATE('15-01-2018','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('RVJ7973','FRASUA',TO_DATE('13-02-2018','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('KBA3785','JUAPAS',TO_DATE('04-03-2018','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('FNV4363','CARGAL',TO_DATE('21-06-2018','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('PQS2578','FRASUA',TO_DATE('24-07-2018','DD-MM-YYYY'),2);
INSERT INTO pide VALUES ('YWQ3299','JUAPAS',TO_DATE('20-08-2018','DD-MM-YYYY'),2);
INSERT INTO pide VALUES ('SFN6569','ROSIBA',TO_DATE('25-08-2018','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('SXW8859','FRASUA',TO_DATE('02-09-2018','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('FJM4476','ADRRED',TO_DATE('15-10-2018','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('LVN2376','ANANAV',TO_DATE('26-10-2018','DD-MM-YYYY'),2);
INSERT INTO pide VALUES ('ZNL9675','IREBLA',TO_DATE('14-11-2018','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('SXW8859','IREBLA',TO_DATE('15-02-2019','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('RVJ7973','LUCHER',TO_DATE('15-02-2019','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('JTS5457','ANANAV',TO_DATE('16-03-2019','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('LVN2376','LUIPAR',TO_DATE('02-04-2019','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('WRG9255','CARGAL',TO_DATE('04-06-2019','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('YWQ3299','ANANAV',TO_DATE('27-06-2019','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('ZVJ5656','JUALOR',TO_DATE('27-06-2019','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('FNV4363','ANANAV',TO_DATE('02-07-2019','DD-MM-YYYY'),0);
INSERT INTO pide VALUES ('YWQ3299','IREBLA',TO_DATE('19-09-2019','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('WRG9255','FRASUA',TO_DATE('06-10-2020','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('SFN6569','ADRRED',TO_DATE('16-11-2019','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('DLT9758','ADRRED',TO_DATE('30-11-2019','DD-MM-YYYY'),2);
INSERT INTO pide VALUES ('ZVJ5656','CARGAL',TO_DATE('03-12-2019','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('FNV4363','JUALOR',TO_DATE('05-12-2019','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('JTS5457','JUALOR',TO_DATE('11-12-2019','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('ZVJ5656','MANGUE',TO_DATE('16-01-2020','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('KBA3785','ADRRED',TO_DATE('20-04-2020','DD-MM-YYYY'),0);
INSERT INTO pide VALUES ('ZVJ5656','JUAPAS',TO_DATE('24-05-2020','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('FJM4476','ANANAV',TO_DATE('19-06-2020','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('FWW4785','ANANAV',TO_DATE('05-08-2020','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('FJM4476','JUAPAS',TO_DATE('14-08-2020','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('DLT9758','CARGAL',TO_DATE('25-09-2020','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('FJM4476','JUALOR',TO_DATE('27-09-2020','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('FNV4363','LUCHER',TO_DATE('13-11-2020','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('DLT9758','JOSSAN',TO_DATE('09-12-2020','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('FNV4363','IREBLA',TO_DATE('16-12-2020','DD-MM-YYYY'),5);
INSERT INTO pide VALUES ('XEM5729','JOSSAN',TO_DATE('31-01-2021','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('SXW8859','JUALOR',TO_DATE('11-02-2021','DD-MM-YYYY'),3);
INSERT INTO pide VALUES ('PQS2578','JUALOR',TO_DATE('18-02-2021','DD-MM-YYYY'),1);
INSERT INTO pide VALUES ('YWQ3299','CARGAL',TO_DATE('28-02-2021','DD-MM-YYYY'),4);
INSERT INTO pide VALUES ('ZVJ5656','LUCHER',TO_DATE('17-04-2021','DD-MM-YYYY'),3);