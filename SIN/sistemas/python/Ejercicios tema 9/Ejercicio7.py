# (7) Create a program that ask a number and shows “YES” if it a prime number, else if it is not.
# A prime number (or a prime) is a natural number greater than 1 that is not a product of two smaller natural numbers.

n = int(input("Insert a positive number to see if is prime or not, different from 0 or 1: "))
while n < 2:
    if (n == 0 or n == 1):
         n = int(input("I said different, try again, please: "))
    else:
        n = int(input("I said positive, try again, please:"))
        
i = 2
message = "YES"
while i != n:
    if (n % i == 0):
        message = "not"
    i += 1
print(message)
