# (5) Write a program that reads a number N and display the associated pattern
# like right angle triangle using an asterisk.
# For example, for N=4:
# *
# **
# ***
# ****

n = int (input ("Insert the number of rows for make a triangle: "))
count = 0
ast = "*"
while n < 0:
    print("Man, you have to put a positive number.")
    n = int (input ("Try again: "))
while count < n:
    print(f"\n{ast}")
    count += 1
    ast += "*"
