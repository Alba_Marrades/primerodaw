# (12) Write a program to convert a decimal number to binary number.

import math
decimal = float(input("Insert a decimal number to convert it to a binary number: "))

# Separates the integer part form the decimal part
decimal_part, integer_part = math.modf(decimal)
integer_part = int(integer_part)

# Convert integer part:
ibinaryi = ""
while integer_part // 2 != 0:
    ibinaryi = str(integer_part % 2) + ibinaryi
    integer_part = integer_part // 2
ibinary = str(integer_part) + ibinaryi

# Convert decimal part
dbinaryi = ""
while decimal_part * 2 != 1:
    decimal_part = decimal_part * 2
    decimal_part2, integer_part2 = math.modf(decimal_part)
    integer_part2 = int(integer_part2)
    dbinaryi += str(integer_part2)
    decimal_part = decimal_part2
decimal_part = int(decimal_part * 2)
dbinary = dbinaryi + str(decimal_part)

# Match the integer part with the decimal part
binary = ibinary + "'" + dbinary

print(binary)