#!/usr/bin/python3
import subprocess
from datetime import datetime

#Function to validate the user.
def validate_user(username):
        
    userExistCommand = f"grep {username} /etc/passwd" #If the user exists shows it.

    #I launch the command and get normal output and error output of the executed command.
    p = subprocess.Popen(userExistCommand, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8.

    #If there is an error:
    if (err != None):
    	print("Error serching user") 
    	return False

    #If username is in MESSAGE:
    if (MESSAGE.find(username) != -1):

        #Get found username and save it into user.
        lines = MESSAGE.split(":") 
        user = lines[0] 

        #If user is the same:
        if (user == username):
            return True
    
    #If not found username:
    print("That user does not exist.")
    return False 
        
    
idCommand = "id" #Shows the id of the user, the id of the group and the membership.

#I launch the command and get normal output and error output of the executed command.
p = subprocess.Popen(idCommand, stdout=subprocess.PIPE, shell=True)
(output, err) = p.communicate()
MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8.

#If the user is part of the sudo group:
if (MESSAGE.find("sudo") != -1):

    username = input("Tell me your username: ") #Ask for the username and insert it into username.
    validUser = validate_user(username)

    #While the user name does not exist:
    while (validUser == False):
        
        username = input("Tell me your username: ") #Ask for the username and insert it into username.
        validUser = validate_user(username)

    userFolderCommand = "echo $HOME" #Shows the folder of the user.
   
    #I launch the command and get normal output and error output of the executed command.
    p = subprocess.Popen(userFolderCommand, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8.
    pathUserFolder = MESSAGE.rstrip() #Folder path name without line break.
       
    #If the directory exists:
    if (MESSAGE.find(username) != -1):
        
        nameUserFolder = pathUserFolder[6:] #Name of the user folder.

        fileExistCommand = f"find {pathUserFolder} -name nobackup" #If the file exists shows it.

        #I launch the command and get normal output and error output of the executed command.
        p = subprocess.Popen(fileExistCommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8.

        countFilesCommand = "ls -a | wc -l" #Counts the files and the directories including the hidden ones of the current directory.

        #I launch the command and get normal output and error output of the executed command.
        p = subprocess.Popen(countFilesCommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        MESSAGE2=output.decode("utf-8") #Bytes passing to String UTF8.
        number = int(MESSAGE2.rstrip()) #Remove the line break from the str and parse the str to int.
       

        #If the file doesn't exist and the number of files and directories is even:
        if (MESSAGE.find("nobackup") == -1 and number % 2 == 0):

            now = datetime.now() #Current date.
            
            date = now.strftime("%d-%m-%Y.%H-%M-%S") #Format current date.

            zipName = f"{nameUserFolder}.{date}.zip" #Name of the zip.
         
            #Compresses the {nameUserFolder} folder with the {nameUserFolder}.{date}.zip name.
            compressCommand = f"zip -r {zipName} {pathUserFolder}"
            
            print(f"Compressing {zipName}...")
            #I launch the command and get normal output and error output of the executed command.
            p = subprocess.Popen(compressCommand, stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()

            fileExistCommand = f"find {pathUserFolder} -name {zipName}"

            #I launch the command and get normal output and error output of the executed command.
            p = subprocess.Popen(fileExistCommand, stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8.

            if (MESSAGE.find(zipName) != -1):
                print("Directory compressed.")
           
            goToUserFolderCommand = f"cd {pathUserFolder}" #Takes you to the user folder.
                        
            #I launch the command and get normal output and error output of the executed command.
            p = subprocess.Popen(goToUserFolderCommand, stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            
            sharedSambaFolder = f"{pathUserFolder}/sharedsamba" 
            moveFolderCommand = f"mv {zipName} {pathUserFolder}/sharedsamba" #Move the zip to the sharedsamba folder.
                        
            #I launch the command and get normal output and error output of the executed command.
            p = subprocess.Popen(moveFolderCommand, stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()

            fileExistCommand = f"find {sharedSambaFolder} -name {zipName}"
            
            #I launch the command and get normal output and error output of the executed command.
            p = subprocess.Popen(fileExistCommand, stdout=subprocess.PIPE, shell=True)
            (output, err) = p.communicate()
            MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8.
            
            if (MESSAGE.find(zipName) != -1):
                print(f"The folder is located in {sharedSambaFolder}.")
            
            
        #The file exist or the number of files and directories is odd:
        else:
            errorMessage = "ERROR: The nobackup file exist or the number of files and directories is odd."
            print(errorMessage)

    #The directory does not exist:
    else: 
        errorMessage = "ERROR: the directory does not exist."
        print(errorMessage)

#You are not root:
else:
    errorMessage = "ERROR: you are not root."
    print(errorMessage)
