#!/usr/bin/python3
import subprocess
import time

TOKEN="1677583075:AAH-XOJMVf_4zxCJXcs6ven1IdduviaVWkY" #The token of the bot.
RECEIVER_ID="357843877"  #The id of the bot.
#The status of the samba service.
statusCommand="sudo service smbd status"

URL_SEND="https://api.telegram.org/bot"+TOKEN+"/sendMessage" #URL API Web to submit

while(True): 

	#I launch the command and get normal output and error output of the executed command.
	p = subprocess.Popen(statusCommand, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	MESSAGE=output.decode("utf-8")  #Bytes passing to String UTF8
	lines = MESSAGE.splitlines() #Insert into an array the lines of the message.
	status = lines[2] #Save into status the line of the status in the message.
	inactive = status.find("inactive") #Return -1 if the samba service is inactive.
	
	#If the samba service is inactive notify telegram.
	if (inactive != -1):	
		#Send by Post to the given URL, to the chat with the user ID the message.
		command="curl -s -X POST "+URL_SEND+" -d chat_id="+RECEIVER_ID+" -d text='"+str("SAMBA SERVICE INACTIVE\n" + status[inactive:])+"'"
		#I launch the command and get normal output and error output of the executed command.
		p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
		(output, err) = p.communicate()
		
	#We stop 1 minute.
	time.sleep(60)