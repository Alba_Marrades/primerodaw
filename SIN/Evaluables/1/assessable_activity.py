price = 0
print("Model's name: \nWhite\nRed\nBlack")
model = input("Could you enter the model name, please?: ").capitalize()
if (model == "White"):
    price = 250
elif (model == "Red"):
    price = 280
elif (model == "Black"):
    price = 300

price_w_discount = price - (price * 0.05)
if (model == "White" or model == "Red" or model == "Black"):
    discount = input("Could you enter the discount price, please?: ")
    if (discount == "inenglishplease"):
        print(model + f" Price: {price}€ \033[0;32;40m Price with 5% discount: {price_w_discount}€")
    else:
        print(model + f" Price: {price}€")
    
else:
    print("\033[0;37;41m ERROR: That model doesn't exist, sorry!")

