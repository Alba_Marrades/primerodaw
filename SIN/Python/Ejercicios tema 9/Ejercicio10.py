# (10) Write a program to display the pattern like pyramid using the alphabet. The program requests for the number of rows. 
#Sample Output:
#    A
#   ABA
#  ABCBA
# ABCDCBA
#ABCDEDCBA

# Variable to save what the user inserts by keyboard.
rows = int(input("Insert the number of rows (Maximum 26), please: "))
# Loop for control that the user doesn't inserts a number which the program can't use.
while rows > 26 or rows < 0:
    rows = int(input("I can't make a pyramid with that number of rows, try again: "))

# Array of letters.
letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J','K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V' 'W', 'X', 'Y', 'Z']
# Loop that iterates the rows of the pyramid.
for i in range(rows):
    # Variable row, it cleans itself in each row of the pyramid to start over.
    row = ""
    # Nested loops to iterate the columns of the row:
    # Loop for the spaces.
    for k in range(rows - i + 1):
        row += " "
    # Loop for the letters
    for j in range(i + 1):
        row += letters[j]
    # Loop for the inverted letters.
    for l in range(i):
        row += letters[i - l - 1]
    # Print the row. 
    print(row)