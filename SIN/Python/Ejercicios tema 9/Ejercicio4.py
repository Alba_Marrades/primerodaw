# (4) Repeat last exercise again using while loop.

n = int(input("Insert the number of numbers you want the program reads, please: "))
count = 0
m = int(input("Insert a number, please: "))
max = min = m
while count < n - 1:
    m = int(input("Insert a number, please: "))
    count += 1
    if (m > max):
        max = m
    if (m < min):
        min = m
print(f"The maximum number is: {max}\nThe minimum number is: {min}")
