# (9) Create a program that asks indefinitely for a text string. For each one of
# them, a folder will be created inside the PYB4EX9 which name will be the string.
# The request will be made until the directory name was END (in capital letters).

import os
string = input("Enter a text string to name the directory we are creating: ")
while string != "END":
    os.makedirs(f'PYB4EX9/{string}', exist_ok=True)
    string = input("Enter a text string to name the directory we are creating: ")