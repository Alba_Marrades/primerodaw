# (13) Write a calculator that allows conversion between number systems.
# Sample:
# 1. Decimal to binary
# 2. Binary to decimal
# 3. Decimal to hexadecimal
# 4. Hexadecimal to decimal
# 5. Binary to hexadecimal
# 6. Hexadecimal to binary
# 7. Exit
# Select an option: 1
# Number: 54
# 23 (10 = 110110 (2
# 1. Decimal to binary
# 2. Binary to decimal
# 3. Decimal to hexadecimal
# 4. Hexadecimal to decimal
# 5. Binary to hexadecimal
# 6. Hexadecimal to binary
# 7. Exit
# Select an option: 7
# Bye!

# Menu:
# while not exit:
#     print("1. Decimal to binary")
#     print("2. Binary to decimal")
#     print("3. Decimal to hexadecimal")
#     print("4. Hexadecimal to decimal")
#     print("5. Binary to hexadecimal")
#     print("6. Hexadecimal to binary")
#     print("7. Exit")

#     option = int(input("Select an option: "))

#     if (option == 1):
#         # 1. Decimal to binary
#         import math
#         decimal = float(input("Insert a decimal number to convert it to a binary number: "))

#         # Separates the integer part form the decimal part
#         decimal_part, integer_part = math.modf(decimal)
#         integer_part = int(integer_part)

#         # Convert integer part:
#         ibinaryi = ""
#         while integer_part // 2 != 0:
#             ibinaryi = str(integer_part % 2) + ibinaryi
#             integer_part = integer_part // 2
#         ibinary = str(integer_part) + ibinaryi

#         # Convert decimal part:
#         dbinaryi = ""
#         while decimal_part * 2 != 1:
#             decimal_part = decimal_part * 2
#             decimal_part2, integer_part2 = math.modf(decimal_part)
#             integer_part2 = int(integer_part2)
#             dbinaryi += str(integer_part2)
#             decimal_part = decimal_part2
#         decimal_part = int(decimal_part * 2)
#         dbinary = dbinaryi + str(decimal_part)

#         # Match the integer part with the decimal part
#         binary = ibinary + "'" + dbinary

#         print(binary)
    
#     elif (option == 2):
        # # 2. Binary to decimal
        # binary = (input("Insert a binary number to convert it to a decimal number: "))

        # # Separates the integer part form the decimal part
        # parts = binary.split('\'')

        # integer_part = parts[0]
        # decimal_part = parts[1]

        # # Convert integer part:
        # # Invert the number
        # integer_parti = integer_part[::-1]
        # decimal_integer_part = 0
        # for i in range (len(integer_parti)):
        #     decimal_integer_part += int(integer_parti[i]) * 2 ** i 

        # # Convert decimal part:
        # decimal_decimal_part = 0
        # for i in range (len(decimal_part)):
        #     decimal_decimal_part += float(decimal_part[i]) * 2 ** ((i + 1) * -1)

        # # Match the integer part with the decimal part
        # decimal = decimal_integer_part + decimal_decimal_part

        # print(decimal)
    elif (option == 3):
        # 3. Decimal to hexadecimal